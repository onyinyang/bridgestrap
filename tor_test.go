package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"testing"
	"time"
)

func TestWriteConfigToTorrc(t *testing.T) {

	dataDir := "/foo"
	fileBuf := new(bytes.Buffer)
	torrc := `UseBridges 1
ControlPort unix:/foo/control-socket
SocksPort auto
SafeLogging 0
Log notice file /foo/tor.log
DataDirectory /foo
ClientTransportPlugin obfs2,obfs3,obfs4,scramblesuit exec /usr/bin/obfs4proxy -enableLogging -logLevel DEBUG
ClientTransportPlugin webtunnel exec ./webtunnel
Bridge obfs4 192.95.36.142:443 CDF2E852BF539B82BD10E27E9115A31734E378C2 cert=qUVQ0srL1JI/vO6V6m/24anYXiJD3QP2HgzUKQtQ7GRqqUvs7P+tG43RtAqdhLOALP7DJQ iat-mode=1
Bridge obfs4 193.11.166.194:27015 2D82C2E354D531A68469ADF7F878FA6060C6BACA cert=4TLQPJrTSaDffMK7Nbao6LC7G9OW/NHkUwIdjLSS3KYf0Nv4/nQiiI8dY2TcsQx01NniOg iat-mode=0
Bridge obfs4 37.218.245.14:38224 D9A82D2F9C2F65A18407B1D2B764F130847F8B5D cert=bjRaMrr1BRiAW8IE9U5z27fQaYgOhX1UCmOpg2pFpoMvo6ZgQMzLsaTzzQNTlm7hNcb+Sg iat-mode=0
`
	err := writeConfigToTorrc(fileBuf, dataDir, "/usr/bin/obfs4proxy", "./webtunnel")
	if err != nil {
		t.Errorf("Failed to write config to torrc: %s", err)
	}

	if torrc != fileBuf.String() {
		t.Errorf("Torrc is not as expected.")
	}
}

func TestGetBridgeIdentifier(t *testing.T) {

	bridgeLine := "obfs4 37.218.245.14:38224 D9A82D2F9C2F65A18407B1D2B764F130847F8B5D cert=bjRaMrr1BRiAW8IE9U5z27fQaYgOhX1UCmOpg2pFpoMvo6ZgQMzLsaTzzQNTlm7hNcb+Sg iat-mode=0"
	identifier, err := getBridgeIdentifier(bridgeLine)
	if err != nil || identifier != "$D9A82D2F9C2F65A18407B1D2B764F130847F8B5D" {
		t.Errorf("failed to extract bridge identifier")
	}

	// Let's try again but this time without fingerprint.
	bridgeLine = "obfs4 37.218.245.14:38224 cert=bjRaMrr1BRiAW8IE9U5z27fQaYgOhX1UCmOpg2pFpoMvo6ZgQMzLsaTzzQNTlm7hNcb+Sg iat-mode=0"
	identifier, err = getBridgeIdentifier(bridgeLine)
	if err != nil || identifier != "37.218.245.14:38224" {
		t.Errorf("failed to extract bridge identifier")
	}
}

func TestObfs4BridgeTest(t *testing.T) {
	obfs4bridges, err := builtinBridges("obfs4")
	if err != nil {
		t.Fatal(err)
	}

	bogusBridge := "127.0.0.1:1"
	testBridges(t, []string{obfs4bridges[0], obfs4bridges[1], bogusBridge}, []bool{true, true, false})
}

func TestNoFingerprintBridges(t *testing.T) {
	obfs4bridges, err := builtinBridges("obfs4")
	if err != nil {
		t.Fatal(err)
	}

	re := regexp.MustCompile("^(obfs4 [^ ]*) [^ ]* ")
	bridge1 := re.ReplaceAllString(obfs4bridges[0], "${1} ")
	bridge2 := re.ReplaceAllString(obfs4bridges[2], "${1} ")

	testBridges(t, []string{bridge1, bridge2}, []bool{true, true})
}

func builtinBridges(tpe string) ([]string, error) {
	resp, err := http.Get("https://bridges.torproject.org/moat/circumvention/builtin")
	if err != nil {
		return nil, fmt.Errorf("Can't fetch builtin bridges: %s", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Unexpected status code: %s", resp.Status)
	}
	d := json.NewDecoder(resp.Body)
	builtinBridges := make(map[string][]string)
	d.Decode(&builtinBridges)
	bridges, ok := builtinBridges[tpe]
	if !ok || len(bridges) == 0 {
		return nil, fmt.Errorf("There is no %s bridges", tpe)
	}
	return bridges, nil
}

func TestWebtunnelBridge(t *testing.T) {
	// From: https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/webtunnel/
	bridge := "webtunnel 192.0.2.3:1 url=https://n97jlbvjwy3iotetc0axrtu3mm7gq1gajqv7-80-78-24-44.sslip.io/939e42a2-8f3f-4645-9d7b-8b9be28b157e"
	testBridges(t, []string{bridge}, []bool{true})
}

func testBridges(t *testing.T, bridges []string, functional []bool) {
	TorTestTimeout = time.Minute
	torCtx = &TorContext{
		TorBinary:        "tor",
		Obfs4proxyBinary: "/usr/bin/obfs4proxy",
		WebtunnelBinary:  "./webtunnel",
	}
	if err := torCtx.Start(); err != nil {
		t.Fatalf("Failed to start tor: %s", err)
	}

	resultChan := make(chan *TestResult)
	req := &TestRequest{
		BridgeLines: bridges,
		resultChan:  resultChan,
	}
	// Submit the test request.
	torCtx.RequestQueue <- req
	// Now wait for the test result.
	result := <-resultChan

	for i, b := range bridges {
		r, _ := result.Bridges[b]
		if r.Functional != functional[i] {
			t.Errorf("Bridge %s was expected functional %t: %v", b, functional[i], r.Error)
		}
	}

	if err := torCtx.Stop(); err != nil {
		t.Fatalf("Failed to stop tor: %s", err)
	}
}
