                      Bridgestrap metrics (version 1)

Metrics data from the Bridgestrap can be retrieved by sending an HTTP GET request
to https://[Bridgestrap URL]/collector. They are produced every 24 hours and
consists of the following items:

    "bridgestrap-stats-end" YYYY-MM-DD HH:MM:SS (NSEC s) NL
        [At start, exactly once.]

        YYYY-MM-DD HH:MM:SS defines the end of the included measurement
        interval of length NSEC seconds (86400 seconds by default).

    "bridgestrap-cached-requests" NUM NL
        [At most once.]

        A count of the total number of requests that were already in the cache.

    "bridgestrap-test" FUNCTIONAL [HASH] NL
        [Any number.]

        FUNCTIONAL is a boolean value ("true" or "false") if the test was
        sucessful or not.

        HASH is the hex representation of the sha1 hash of the bridge
        fingerprint. It will not be present if the bridge is not functional and
        no fingerprint was provided in the bridge line.
